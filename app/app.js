var express = require('express');
var app = express();

// Routes
app.get('/', function(req, res) {
  res.send('Hello World!');
});

app.get("/file", function(req, res) {
  res.download("/tmp/myfile");
});

// Listen
var port = process.env.PORT || 5000;
console.log("Starting hello-world server");
app.listen(port, () => console.log('Listening on localhost:'+ port));

